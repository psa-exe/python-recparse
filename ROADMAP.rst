.. vim: et: sw=3: ts=3: tw=100:
   vim: filetype=rst:
   vim: spell: spelllang=en:

Library development roadmap
===========================

Planned features include the ability to specify record types building on other record types previously defined, e.g.

.. code:: python

    from recparse import Array, FieldSpec, Record
    from recparse.types import *


    class Header(Record):
        version = Uint32
        author = String(30)
        timestamp = Uint64
        comment = String(100, encoding='ascii')
        channels = Uint8
        PADDING = 10


    class DataChannelHeader(Record):
        name = String(32)
        frequency = Float32
        comment = String(100)


    class Data(Record):
        id = Uint32
        length = Uint8
        samples = Array(Float32, length=length)


    class File(Record):
        header = Header
        data_headers = Array(DataChannelHeader, length=header.channels)
        entries = Array(Data)

notice that in the case of arrays, it is useful to specify the number of elements in terms of an expression involving
other field previously defined.
