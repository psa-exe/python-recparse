# -*- coding: utf-8 -*-
#
# recparse - Declarative parsing of records in binary files
# Copyright (C) 2018  Pedro Asad
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <http://www.gnu.org/licenses/>.
#
# If you have any questions or suggestions regarding this software, visit
# <http://gitlab.com/psa-exe/recparse/>.

import collections
import pytest
import recparse.types
import struct


class Test__RecordArray:
    @pytest.fixture(scope='class')
    def record_type(self):
        class RecordType(recparse.Record):
            a = recparse.FieldSpec(recparse.types.Int32)
            b = recparse.FieldSpec(recparse.types.Float32)
            c = recparse.FieldSpec(recparse.types.Uint16)

        return RecordType

    @pytest.fixture(scope='class')
    def record_instances(self, record_type):
        return [
            record_type(a=400, b=-64.25, c=2**15),
            record_type(a=-50, b=-720.5, c=20915),
            record_type(a=0xA, b=478976, c=36415),
        ]

    def test__constructor__from_iterable(self, record_type, record_instances):
        record_array = recparse.RecordArray.from_iterable(record_instances, record_type=record_type)

        assert len(record_array) == len(record_instances)

        for i in range(len(record_instances)):
            assert record_array[i] == record_instances[i]

        assert bytes(record_array) == b''.join(bytes(rec) for rec in record_instances)

    def test__constructor__from_stream(self, record_type, record_instances, tmpdir):
        path = str(tmpdir.join('record_array.bin'))
        with open(path, 'wb') as file:
            for record in record_instances:
                file.write(bytes(record))

        with open(path, 'rb') as file:
            record_array = recparse.RecordArray.from_stream(file, record_type=record_type)

        assert len(record_array) == len(record_instances)

        for i in range(len(record_instances)):
            assert record_array[i] == record_instances[i]

        assert bytes(record_array) == b''.join(bytes(rec) for rec in record_instances)

    def test__operation__delitem(self, record_type, record_instances):
        record_array = recparse.RecordArray.from_iterable(record_instances, record_type=record_type)

        del record_array[1]

        assert record_array[0] == record_instances[0]
        assert record_array[1] == record_instances[2]

    def test__operation__equality(self, record_type, record_instances):
        record_array1 = recparse.RecordArray.from_iterable(record_instances, record_type=record_type)
        record_array2 = recparse.RecordArray.from_iterable(record_instances, record_type=record_type)

        assert record_array1 == record_array2

    def test__operation__setitem(self, record_type, record_instances):
        record_array = recparse.RecordArray.from_iterable(record_instances, record_type=record_type)

        record_array[0] = record_array[2]

        assert record_array[0] == record_array[2]


class Test__Record__with_just_fields:
    @pytest.fixture(scope='class')
    def record_type(self):
        class RecordType(recparse.Record):
            a = recparse.FieldSpec(recparse.types.Int32)
            b = recparse.FieldSpec(recparse.types.Float32)
            c = recparse.FieldSpec(recparse.types.Uint16)
        return RecordType

    def test__class_method__fields(self, record_type):
        fields = collections.OrderedDict([
            ('a', recparse.FieldSpec(name='a', type=recparse.types.Int32, offset=0)),
            ('b', recparse.FieldSpec(name='b', type=recparse.types.Float32, offset=4)),
            ('c', recparse.FieldSpec(name='c', type=recparse.types.Uint16, offset=8)),
        ])

        assert record_type.fields() == fields

    def test__class_method__padding(self, record_type):
        assert record_type.padding() == 0

    def test__class_method__struct_format(self, record_type):
        assert record_type.struct_format() == 'ifH'

    def test__class_method__total_size(self, record_type):
        assert record_type.total_size() == 10

    def test__class_method__used_size(self, record_type):
        assert record_type.used_size() == 10

    def test__constructor__default__without_buffer(self, record_type):
        a, b, c = attributes = (-2 ** 31, 1234.5, 2 ** 15 - 1)
        mapping = {'a': a, 'b': b, 'c': c}

        source_bytes = struct.pack('ifH', *attributes)
        record = record_type(**mapping)

        assert record['a'] == a
        assert record['b'] == b
        assert record['c'] == c

        assert len(record) == 3
        assert bytes(record) == source_bytes

        assert list(record.keys()) == 'a b c'.split()
        assert tuple(record.values()) == attributes
        assert list(record.items()) == list(zip('a b c'.split(), attributes))

    def test__constructor__default_empty(self, record_type, tmpdir):
        a, b, c = attributes = struct.unpack('ifH', bytes(10))

        record = record_type()

        assert record['a'] == a
        assert record['b'] == b
        assert record['c'] == c

        assert len(record) == 3
        assert bytes(record) == bytes(10)

        assert list(record.keys()) == 'a b c'.split()
        assert tuple(record.values()) == attributes
        assert list(record.items()) == list(zip('a b c'.split(), attributes))

    @pytest.mark.parametrize('bytes_type', [bytes, bytearray])
    def test__constructor__from_bytes(self, record_type, bytes_type):
        a, b, c = attributes = (-2**31, 1234.5, 2**15 - 1)

        source_bytes = struct.pack('ifH', *attributes)
        record = record_type.from_bytes(bytes_type(source_bytes))

        assert record['a'] == a
        assert record['b'] == b
        assert record['c'] == c

        assert len(record) == 3
        assert bytes(record) == source_bytes

        assert list(record.keys()) == 'a b c'.split()
        assert tuple(record.values()) == attributes
        assert list(record.items()) == list(zip('a b c'.split(), attributes))

    def test__constructor__from_iterable(self, record_type):
        a, b, c = attributes = (-2 ** 31, 1234.5, 2 ** 15 - 1)

        source_bytes = struct.pack('ifH', *attributes)
        record = record_type.from_iterable(attributes)

        assert record['a'] == a
        assert record['b'] == b
        assert record['c'] == c

        assert len(record) == 3
        assert bytes(record) == source_bytes

        assert list(record.keys()) == 'a b c'.split()
        assert tuple(record.values()) == attributes
        assert list(record.items()) == list(zip('a b c'.split(), attributes))

    def test__from_stream__constructor(self, record_type, tmpdir):
        a1, b1, c1 = attributes1 = (-2 ** 31, 1234.5, 2 ** 15 - 1)
        a2, b2, c2 = attributes2 = (2 ** 31 - 1, -1234.5, 800)

        source_bytes1 = struct.pack('ifH', *attributes1)
        source_bytes2 = struct.pack('ifH', *attributes2)
        source_path = tmpdir.join('record.bin')

        with open(str(source_path), 'wb') as source_file:
            source_file.write(source_bytes1)
            source_file.write(source_bytes2)

        with open(str(source_path), 'rb') as source_file:
            record1 = record_type.from_stream(source_file)
            record2 = record_type.from_stream(source_file)

        assert record1['a'] == a1 and record2['a'] == a2
        assert record1['b'] == b1 and record2['b'] == b2
        assert record1['c'] == c1 and record2['c'] == c2

        assert len(record1) == 3 and len(record2) == 3
        assert bytes(record1) == source_bytes1 and bytes(record2) == source_bytes2

        keys = 'a b c'.split()
        items1 = list(zip(keys, attributes1))
        items2 = list(zip(keys, attributes2))

        assert list(record1.keys()) == keys
        assert tuple(record1.values()) == attributes1 and tuple(record2.values()) == attributes2
        assert list(record1.items()) == items1 and list(record2.items()) == items2

    def test__operation__repr(self, record_type):
        a, b, c = (-2 ** 31, 1234.5, 2 ** 15 - 1)

        record1 = record_type(a=a, b=b, c=c)

        assert repr(record1) == f'{record_type.__name__}(a={a!r}, b={b!r}, c={c!r})'

    def test__operation__equality(self, record_type):
        a, b, c = (-2 ** 31, 1234.5, 2 ** 15 - 1)

        record1 = record_type(a=a, b=b, c=c)
        record2 = record_type(a=a, b=b, c=c)

        assert record1 == record2

        record1['a'] = 9

        assert record1 != record2

    def test__operation__item_assignment(self, record_type):
        attributes1 = (-2 ** 31, 1234.5, 2 ** 15 - 1)
        a2, b2, c2 = attributes2 = (2 ** 31 - 1, -1234.5, 800)

        bytes1 = struct.pack('ifH', *attributes1)
        record = record_type.from_bytes(bytes1)

        bytes2 = struct.pack('ifH', *attributes2)

        record['a'] = a2
        record['b'] = b2
        record['c'] = c2

        assert record['a'] == a2
        assert record['b'] == b2
        assert record['c'] == c2

        assert len(record) == 3
        assert bytes(record) == bytes(bytes2)

        assert list(record.keys()) == 'a b c'.split()
        assert tuple(record.values()) == attributes2
        assert list(record.items()) == list(zip('a b c'.split(), attributes2))


class Test__Record__with_padding:
    @pytest.fixture(scope='class')
    def record_type(self):
        class RecordType(recparse.Record):
            a = recparse.FieldSpec(recparse.types.Int32)
            b = recparse.FieldSpec(recparse.types.Float32)
            c = recparse.FieldSpec(recparse.types.Uint16)
            PADDING = 2
        return RecordType

    def test__class_method__fields(self, record_type):
        fields = collections.OrderedDict([
            ('a', recparse.FieldSpec(name='a', type=recparse.types.Int32, offset=0)),
            ('b', recparse.FieldSpec(name='b', type=recparse.types.Float32, offset=4)),
            ('c', recparse.FieldSpec(name='c', type=recparse.types.Uint16, offset=8)),
        ])

        assert record_type.fields() == fields

    def test__class_method__padding(self, record_type):
        assert record_type.padding() == 2

    def test__class_method__struct_format(self, record_type):
        assert record_type.struct_format() == 'ifH2x'

    def test__class_method__total_size(self, record_type):
        assert record_type.total_size() == 12

    def test__class_method__used_size(self, record_type):
        assert record_type.used_size() == 10

    def test__constructor__default_empty(self, record_type, tmpdir):
        a, b, c = attributes = struct.unpack('ifH', bytes(10))

        record = record_type()

        assert record['a'] == a
        assert record['b'] == b
        assert record['c'] == c

        assert len(record) == 3
        assert bytes(record) == bytes(12)

        assert list(record.keys()) == 'a b c'.split()
        assert tuple(record.values()) == attributes
        assert list(record.items()) == list(zip('a b c'.split(), attributes))

    def test__constructor__default_without_buffer(self, record_type):
        a, b, c = attributes = (-2 ** 31, 1234.5, 2 ** 15 - 1)

        source_bytes = struct.pack('ifH', *attributes) + b'\x00\x00'
        record = record_type(a=a, b=b, c=c)

        assert record['a'] == a
        assert record['b'] == b
        assert record['c'] == c

        assert len(record) == 3
        assert bytes(record) == source_bytes

        assert list(record.keys()) == 'a b c'.split()
        assert tuple(record.values()) == attributes
        assert list(record.items()) == list(zip('a b c'.split(), attributes))

    @pytest.mark.parametrize('bytes_type', [bytes, bytearray])
    def test__constructor__from_bytes(self, record_type, bytes_type):
        a, b, c = attributes = (-2**31, 1234.5, 2**15 - 1)

        source_bytes = struct.pack('ifH', *attributes) + b'\x13\x17'
        record = record_type.from_bytes(bytes_type(source_bytes))

        assert record['a'] == a
        assert record['b'] == b
        assert record['c'] == c

        assert len(record) == 3
        assert bytes(record) == source_bytes

        assert list(record.keys()) == 'a b c'.split()
        assert tuple(record.values()) == attributes
        assert list(record.items()) == list(zip('a b c'.split(), attributes))

    def test__constructor__from_iterable(self, record_type):
        a, b, c = attributes = (-2 ** 31, 1234.5, 2 ** 15 - 1)

        source_bytes = struct.pack('ifH', *attributes) + b'\x00\x00'
        record = record_type.from_iterable(attributes)

        assert record['a'] == a
        assert record['b'] == b
        assert record['c'] == c

        assert len(record) == 3
        assert bytes(record) == source_bytes

        assert list(record.keys()) == 'a b c'.split()
        assert tuple(record.values()) == attributes
        assert list(record.items()) == list(zip('a b c'.split(), attributes))

    def test__from_stream__constructor(self, record_type, tmpdir):
        a1, b1, c1 = attributes1 = (-2 ** 31, 1234.5, 2 ** 15 - 1)
        a2, b2, c2 = attributes2 = (2 ** 31 - 1, -1234.5, 800)

        source_bytes1 = struct.pack('ifH', *attributes1) + b'\x13\x17'
        source_bytes2 = struct.pack('ifH', *attributes2) + b'\x14\x16'
        source_path = tmpdir.join('record.bin')

        with open(str(source_path), 'wb') as source_file:
            source_file.write(source_bytes1)
            source_file.write(source_bytes2)

        with open(str(source_path), 'rb') as source_file:
            record1 = record_type.from_stream(source_file)
            record2 = record_type.from_stream(source_file)

        assert record1['a'] == a1 and record2['a'] == a2
        assert record1['b'] == b1 and record2['b'] == b2
        assert record1['c'] == c1 and record2['c'] == c2

        assert len(record1) == 3 and len(record2) == 3
        assert bytes(record1) == source_bytes1 and bytes(record2) == source_bytes2

        keys = 'a b c'.split()
        items1 = list(zip(keys, attributes1))
        items2 = list(zip(keys, attributes2))

        assert list(record1.keys()) == keys
        assert tuple(record1.values()) == attributes1 and tuple(record2.values()) == attributes2
        assert list(record1.items()) == items1 and list(record2.items()) == items2

    def test__operation__item_assignment(self, record_type):
        attributes1 = (-2 ** 31, 1234.5, 2 ** 15 - 1)
        a2, b2, c2 = attributes2 = (2 ** 31 - 1, -1234.5, 800)

        bytes1 = struct.pack('ifH', *attributes1) + b'\x13\x17'
        record = record_type.from_bytes(bytes1)

        bytes2 = struct.pack('ifH', *attributes2) + b'\x13\x17'

        record['a'] = a2
        record['b'] = b2
        record['c'] = c2

        assert record['a'] == a2
        assert record['b'] == b2
        assert record['c'] == c2

        assert len(record) == 3
        assert bytes(record) == bytes(bytes2)

        assert list(record.keys()) == 'a b c'.split()
        assert tuple(record.values()) == attributes2
        assert list(record.items()) == list(zip('a b c'.split(), attributes2))


class Test__Record__with_size:
    @pytest.fixture(scope='class')
    def record_type(self):
        class RecordType(recparse.Record):
            a = recparse.FieldSpec(recparse.types.Int32)
            b = recparse.FieldSpec(recparse.types.Float32)
            c = recparse.FieldSpec(recparse.types.Uint16)
            SIZE = 14
        return RecordType

    def test__class_method__fields(self, record_type):
        fields = collections.OrderedDict([
            ('a', recparse.FieldSpec(name='a', type=recparse.types.Int32, offset=0)),
            ('b', recparse.FieldSpec(name='b', type=recparse.types.Float32, offset=4)),
            ('c', recparse.FieldSpec(name='c', type=recparse.types.Uint16, offset=8)),
        ])

        assert record_type.fields() == fields

    def test__class_method__padding(self, record_type):
        assert record_type.padding() == 4

    def test__class_method__struct_format(self, record_type):
        assert record_type.struct_format() == 'ifH4x'

    def test__class_method__total_size(self, record_type):
        assert record_type.total_size() == 14

    def test__class_method__used_size(self, record_type):
        assert record_type.used_size() == 10

    def test__constructor__default_empty(self, record_type, tmpdir):
        a, b, c = attributes = struct.unpack('ifH4x', bytes(14))

        record = record_type()

        assert record['a'] == a
        assert record['b'] == b
        assert record['c'] == c

        assert len(record) == 3
        assert bytes(record) == bytes(14)

        assert list(record.keys()) == 'a b c'.split()
        assert tuple(record.values()) == attributes
        assert list(record.items()) == list(zip('a b c'.split(), attributes))

    def test__constructor__default_without_buffer(self, record_type):
        a, b, c = attributes = (-2 ** 31, 1234.5, 2 ** 15 - 1)

        source_bytes = struct.pack('ifH4x', *attributes)
        record = record_type(a=a, b=b, c=c)

        assert record['a'] == a
        assert record['b'] == b
        assert record['c'] == c

        assert len(record) == 3
        assert bytes(record) == source_bytes

        assert list(record.keys()) == 'a b c'.split()
        assert tuple(record.values()) == attributes
        assert list(record.items()) == list(zip('a b c'.split(), attributes))

    @pytest.mark.parametrize('bytes_type', [bytes, bytearray])
    def test__constructor__from_bytes(self, record_type, bytes_type):
        a, b, c = attributes = (-2**31, 1234.5, 2**15 - 1)

        source_bytes = struct.pack('ifH4x', *attributes)
        record = record_type.from_bytes(bytes_type(source_bytes))

        assert record['a'] == a
        assert record['b'] == b
        assert record['c'] == c

        assert len(record) == 3
        assert bytes(record) == source_bytes

        assert list(record.keys()) == 'a b c'.split()
        assert tuple(record.values()) == attributes
        assert list(record.items()) == list(zip('a b c'.split(), attributes))

    def test__constructor__from_iterable(self, record_type):
        a, b, c = attributes = (-2 ** 31, 1234.5, 2 ** 15 - 1)

        source_bytes = struct.pack('ifH4x', *attributes)
        record = record_type.from_iterable(attributes)

        assert record['a'] == a
        assert record['b'] == b
        assert record['c'] == c

        assert len(record) == 3
        assert bytes(record) == source_bytes

        assert list(record.keys()) == 'a b c'.split()
        assert tuple(record.values()) == attributes
        assert list(record.items()) == list(zip('a b c'.split(), attributes))

    def test__from_stream__constructor(self, record_type, tmpdir):
        a1, b1, c1 = attributes1 = (-2 ** 31, 1234.5, 2 ** 15 - 1)
        a2, b2, c2 = attributes2 = (2 ** 31 - 1, -1234.5, 800)

        source_bytes1 = struct.pack('ifH', *attributes1) + b'abcd'
        source_bytes2 = struct.pack('ifH', *attributes2) + b'1234'
        source_path = tmpdir.join('record.bin')

        with open(str(source_path), 'wb') as source_file:
            source_file.write(source_bytes1)
            source_file.write(source_bytes2)

        with open(str(source_path), 'rb') as source_file:
            record1 = record_type.from_stream(source_file)
            record2 = record_type.from_stream(source_file)

        assert record1['a'] == a1 and record2['a'] == a2
        assert record1['b'] == b1 and record2['b'] == b2
        assert record1['c'] == c1 and record2['c'] == c2

        assert len(record1) == 3 and len(record2) == 3
        assert bytes(record1) == source_bytes1 and bytes(record2) == source_bytes2

        keys = 'a b c'.split()
        items1 = list(zip(keys, attributes1))
        items2 = list(zip(keys, attributes2))

        assert list(record1.keys()) == keys
        assert tuple(record1.values()) == attributes1 and tuple(record2.values()) == attributes2
        assert list(record1.items()) == items1 and list(record2.items()) == items2

    def test__operation__item_assignment(self, record_type):
        attributes1 = (-2 ** 31, 1234.5, 2 ** 15 - 1)
        a2, b2, c2 = attributes2 = (2 ** 31 - 1, -1234.5, 800)

        bytes1 = struct.pack('ifH', *attributes1) + b'1234'
        record = record_type.from_bytes(bytes1)

        bytes2 = struct.pack('ifH', *attributes2) + b'1234'

        record['a'] = a2
        record['b'] = b2
        record['c'] = c2

        assert record['a'] == a2
        assert record['b'] == b2
        assert record['c'] == c2

        assert len(record) == 3
        assert bytes(record) == bytes(bytes2)

        assert list(record.keys()) == 'a b c'.split()
        assert tuple(record.values()) == attributes2
        assert list(record.items()) == list(zip('a b c'.split(), attributes2))
