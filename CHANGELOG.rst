=========
Changelog
=========

All notable changes to this project will be documented in this file. The format is based on `Keep a
Changelog`_ (but uses restructuredText_ instead of *Markdown*), and this project adheres to
`Semantic Versioning`_.

[Unreleased]
============
Added
-----

- Project skeleton (2018-12-28)

  - `README <README.rst>`_ and `CHANGELOG <CHANGELOG.rst>`_ files
  - Python `recparse module <src/recparse.py>`_
  - Tox_-based `test suite <tests/test_recparse>`_
  - Sphinx_-based `documentation <docs>`_
  - bumpversion_ and Poetry_ packaging configuration files
  - `GitLab CI`_ configuration (`.gitlab-ci.yml <.gitlab-ci.yml>`_)

.. _GitLab CI: https://docs.gitlab.com/ee/ci/
.. _Keep a Changelog: https://keepachangelog.com/en/1.0.0/
.. _Poetry: https://poetry.eustace.io/
.. _Semantic Versioning: https://semver.org/spec/v2.0.0.html
.. _Sphinx: http://www.sphinx-doc.org/en/master/
.. _Tox: https://tox.readthedocs.io/en/latest/

.. _bumpversion: https://github.com/peritus/bumpversion
.. _restructuredText: http://docutils.sourceforge.net/rst.html
