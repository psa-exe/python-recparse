.. vim: et: sw=3: ts=3: tw=100:
   vim: filetype=rst:
   vim: spell: spelllang=en:

=========================================================
recparse - Declarative parsing of records in binary files
=========================================================

.. image:: https://img.shields.io/badge/status-prealpha-orange.svg
    :target: https://gitlab.com/psa-exe/python-recparse/tree/0.1.0

.. image:: https://img.shields.io/badge/version-0.1.0-orange.svg
    :target: https://gitlab.com/psa-exe/python-recparse/tree/0.1.0

.. image:: https://img.shields.io/badge/license-GPL3.0-blue.svg
    :target: https://www.gnu.org/licenses/gpl-3.0.en.html

.. image:: https://img.shields.io/badge/Python->=3.6-blue.svg
    :target: https://docs.python.org/3.6/

.. image:: https://gitlab.com/psa-exe/python-recparse/badges/master/pipeline.svg
    :target: https://gitlab.com/psa-exe/python-recparse/badges/master/pipeline.svg

.. image:: https://gitlab.com/psa-exe/python-recparse/badges/master/coverage.svg
    :target: https://gitlab.com/psa-exe/python-recparse/badges/master/coverage.svg


Introduction
============

This is a library for parsing binary files in Python using a *declarative approach*, that is, instead of writing
imperative code like

.. code:: pycon

    >>> import struct
    >>> record_format = 'ifH'  # 32-bit signed integer, 32-bit float, 16-bit unsigned (short) integer
    >>> with open('file.bin', 'rb') as binary_file:
    ...     record_bytes = binary_file.read(struct.calcsize(record_format))
    >>> record_fields = struct.unpack(record_format, record_bytes)
    >>> print(record_fields)
    (-45, 65.87, 451)

you define the fields in your record and parse them with

.. code:: pycon

    >>> from recparse import FieldSpec, Record
    >>> from recparse.types import *
    >>> class RecordType(Record):
    ...     FIELDS = [
    ...         a = FieldSpec(Int32),
    ...         b = FieldSpec(Float32),
    ...         c = FieldSpec(Uint16),
    ...     ]
    >>> with open('file.bin', 'rb') as binary_file:
    ...     record = RecordType.from_stream(binary_file)
    >>> print(record)
    RecordType(a=-45, b=65.87, c=451)

While this initial example may be a bit more verbose, it is less error prone and much more mnemonic.
Visit the `full documentation`_ for more information.


Installation
============

This package's dependencies are managed with Poetry_, a Python packaging solution and dependency
management tool. That means you should install Poetry_ (preferably in a virtualenv_) for installing
the library, since it is still not published on PyPI_.  Installation should be as simple as

.. code:: bash

    poetry install

which will create a virtualenv_, install all dependencies (including development dependencies,
unless you pass the ``--no-dev`` flag to ``poetry install``), assuming you are already running in an
environment where Poetry_ is installed.  From here, you can spawn a subshell with the just-created
virtualenv_ with ``poetry shell`` or run any command with ``poetry run <command>``.

.. warning::
    Make sure to install Poetry_ >= 0.12.0, because there was a `bug
    <poetry_transitive_dep_changelog_>`_ before this version in which transitive local dependencies
    had incorrectly resolved paths during installation. It also seems that ``pyproject.lock`` files
    were permanently renamed to ``poetry.lock`` in some intermediate version.


Building the docs
=================

The documentation is written in Sphinx_ and contained in the ``docs`` directory. To compile, install
the extra dependencies, then compile with ``make``, like so:

.. code:: bash

    poetry install -E docs
    poetry run make -C docs html

which will get you the docs in ``docs/build/html``.


Running the tests
=================

There is a ``pytest``-based test suite under the ``tests`` directory. The recommended way of running
the test suite is using tox_ (or detox_), which automates testing against multiple Python versions
and grants deterministic, isolated test environments. Install tox_ with ``pip install tox`` or ``pip
install --user tox`` if you haven't already and simply run ``tox`` (or ``tox -e py36`` to test only
in a ``py36`` environment, for instance).


Future directions
=================

Check out the `ROADMAP.rst <ROADMAP.rst>`_ file for hints on the planned directions for this library.


.. _Poetry: https://poetry.eustace.io/
.. _PyPI: https://pypi.org/
.. _Sphinx: http://www.sphinx-doc.org/en/master/
.. _detox: https://github.com/tox-dev/detox
.. _full documentation: https://psa-exe.gitlab.io/python-recparse
.. _poetry_transitive_dep_changelog: https://github.com/sdispater/poetry/blob/master/CHANGELOG.md#fixed-11
.. _tox: https://tox.readthedocs.io/en/latest/
.. _virtualenv: https://virtualenv.pypa.io/en/stable/

